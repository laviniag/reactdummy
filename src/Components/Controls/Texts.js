import React, {Component} from 'react';

class Texts extends Component{
    constructor(props){
      super(props);
      this.state = {
        value:props.value
      }
    }

    onChange(event){
        this.setState({value: event.target.value}, function(){
            this.props.onChange(this.state.value);
        });
    }

    render(){
      return (
        <div>
          <label>Paragraphs: </label>
          <input type="number" value={this.state.value} onChange={this.onChange.bind(this)} />
        </div>
      );
    }
}

export default Texts;
