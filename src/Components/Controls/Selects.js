import React, {Component} from 'react';

class Selects extends Component{
      constructor(props){
        super(props);
        this.state = {
          value:props.value
        }
      }

    onChange(event){
        this.setState({value: event.target.value}, function(){
            this.props.onChange(this.state.value);
        });
    }

      render(){
        return (
          <div>
            <label>Select format: </label>
              <select onChange={this.onChange.bind(this)}>
                  <option value="text">Text</option>
                  <option value="html">Html</option>
              </select>
          </div>
        );
      }
}

export default Selects;
