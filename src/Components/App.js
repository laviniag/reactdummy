import React, {Component} from 'react';
import Output from './Output';
import Texts from './Controls/Texts';
import Selects from './Controls/Selects';
import axios from 'axios';

class App extends Component{
  constructor(props){
    super(props);
    this.state = {
      paras: 4,
      format: 'text',
      type: 'meat-and-filler',
      text:''
    }
  }
  componentWillMount(){
    this.getText();
  }

  getText(){
    axios.get('https://baconipsum.com/api/?type='+this.state.type+'&paras='+this.state.paras+'&format='+this.state.format)
      .then((response) => {
          this.setState({text: response.data}, function(){
            // console.log(this.state);
          });
      })
      .catch((err) => {
        console.log(err);
      });
  }

    changeParas(number){
        this.setState({paras: number}, this.getText);
    }

    showHtml(format){
        this.setState({format:format}, this.getText);
    }

      render(){
        return (
            <div className="container">
                <h1>DummyText Generator</h1>
                <Output value={this.state.text} />
                <h3>Real Time Options</h3>
                <form>
                    <div>
                        <Texts value={this.state.paras} onChange={this.changeParas.bind(this)} />
                    </div>
                    <div>
                        <Selects value={this.state.format} onChange={this.showHtml.bind(this)} />
                    </div>
                </form>
            </div>
        );
      }
}

export default App;
